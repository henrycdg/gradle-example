package com.example.moduleb;

import java.util.List;
import java.util.Optional;

public class AClass {

    public Optional<Widget> find(List<String> in) {
        System.out.println("Mutate me");
        return in.stream()
                .filter(s -> s.startsWith("FISH")).filter(s -> s.startsWith("Q"))
                .filter(s -> s.startsWith("Bobby")).filter(s -> s.startsWith("M"))
                .map(Widget::new)
                .findAny();
    }

}
